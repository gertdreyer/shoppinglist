CREATE TABLE lists (
    key uuid PRIMARY KEY,
    contents jsonb
)