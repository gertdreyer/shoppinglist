const express = require('express')
const app = express()
const { Client } = require('pg')
const short = require('short-uuid');
const { v4 } = require('uuid')
const cors = require('cors')
var bodyParser = require('body-parser')
var path = require('path')
require('dotenv').config()

const translator = short();
const port = process.env.PORT || 3000
const connectionString = process.env.DATABASE_URL

console.log(connectionString)

const client = new Client({ connectionString, ssl: { rejectUnauthorized: false } })
client.connect()

app.use(express.static(path.join(__dirname, '../svelte/public')));
app.use(bodyParser.json())
app.use(cors())

serveIndex = (req,res) => {res.sendFile(path.join(__dirname, '../svelte/public/index.html'))}
serveIndex.unless = require('express-unless');
//Serve the index file for requests except api requests
app.use(serveIndex.unless({path: [/\/api\//g]}) )
app.get('/api/new/:name', async (req, res) => {
    let newuuid = v4();
    let dbres = await client.query('INSERT INTO lists (key, contents) values ($1::uuid, $2::json)', [newuuid, { name: req.params.name, items: [] }]);
    if (dbres.rowCount == 1) {
        res.send(translator.fromUUID(newuuid));
    } else {
        res.statusCode = 500;
        res.send();
    }
})

app.get('/api/:id', async (req, res) => {
    let longuuid = translator.toUUID(req.params.id);
    const dbres = await client.query('SELECT contents from lists where key=$1::uuid', [longuuid]);
    if (dbres.rowCount == 1) {
        res.send(dbres.rows[0].contents);
    }
    else {
        res.statusCode = 404;
        res.send("The list you requested does not exist");
    }
})

app.post('/api/:id', async (req, res) => {
    let longuuid = translator.toUUID(req.params.id);
    const dbres = await client.query('SELECT count(key) from lists where key=$1::uuid', [longuuid]);
    if (dbres.rows[0].count == 1) {
        console.log(req.body);
        const dbresupdate = await client.query('UPDATE lists SET contents=$1::json where key=$2::uuid', [req.body, longuuid]);
        if (dbresupdate.rowCount == 1) {
            res.send("Updated");
        } else {
            res.statusCode = 500;
            res.send("An unexpected error occured");
        }
    }
    else {
        res.statusCode = 400;
        res.send("The list you requested to update does not exist");
    }

})
app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})

