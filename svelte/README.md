*Looking for a shareable component template? Go here --> [sveltejs/component-template](https://github.com/sveltejs/component-template)*

---

# Svelte Shopping List

This project uses the [Svelte](https://svelte.dev) project template as a starting point. This uses Rollup as a packager by default. It is assumed that you have a copy of node.js installed and thus npm. The Visual Studio Code extension for Svelte is recommended.

Clone this template with:

```bash
npx degit sveltejs/template shoppinglist
cd shoppinglist
```

For some style we will be using [IBM Carbon Design](https://carbon-svelte.vercel.app/) and the page routing utility:
```bash
npm install carbon-components-svelte page rollup-plugin-css-only
```

Install all dependencies:
```bash
npm install
```

Change 
```json
    "start": "sirv public"
```
in package.json to
```json
    "start": "sirv public --single"
```

import css from "rollup-plugin-css-only";
Start the live development server and open [localhost:5000](http://localhost:5000) in your browser. You should see Hello World.

```bash
npm run dev
```




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Building and running in production mode

To create an optimised version of the app:

```bash
npm run build
```

You can run the newly built app with `npm run start`. This uses [sirv](https://github.com/lukeed/sirv), which is included in your package.json's `dependencies` so that the app will work when you deploy to platforms like [Heroku](https://heroku.com).


## Single-page app mode

By default, sirv will only respond to requests that match files in `public`. This is to maximise compatibility with static fileservers, allowing you to deploy your app anywhere.

If you're building a single-page app (SPA) with multiple routes, sirv needs to be able to respond to requests for *any* path. You can make it so by editing the `"start"` command in package.json:

```js
"start": "sirv public --single"
```

## Using TypeScript

This template comes with a script to set up a TypeScript development environment, you can run it immediately after cloning the template with:

```bash
node scripts/setupTypeScript.js
```

Or remove the script via:

```bash
rm scripts/setupTypeScript.js
```

## Deploying to the web

### With [Vercel](https://vercel.com)

Install `vercel` if you haven't already:

```bash
npm install -g vercel
```

Then, from within your project folder:

```bash
cd public
vercel deploy --name my-project
```

### With [surge](https://surge.sh/)

Install `surge` if you haven't already:

```bash
npm install -g surge
```

Then, from within your project folder:

```bash
npm run build
surge public my-project.surge.sh
```
